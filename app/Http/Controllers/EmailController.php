<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ContactMe;
use App\Mail\ContactMe as ContactMePlease;

class EmailController extends Controller
{
    public function show() 
    {
        return view('email.show');
    }
    public function store() 
    {
        request()->validate(['email'=>'required|email']);
        // Mail::raw('It works',function($m) {
        //     $m->to(request('email'))
        //         ->subject('Hello There');
        // });
        Mail::to(request('email'))
            ->send(new ContactMePlease('shirts'));

        return redirect('email')
            ->with('message','Почта отправлена');
    }
}
