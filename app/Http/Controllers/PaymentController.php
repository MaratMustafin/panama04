<?php

namespace App\Http\Controllers;

use App\Notifications\PaymentReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Events\ProductPurchased;
class PaymentController extends Controller
{
    public function home()
    {
        return view('payment.home');
    }
    public function store()
    {
        ProductPurchased::dispatch('toy');
        // event(new ProductPurchased('toy'));
        // Notification::send(request()->user(), new PaymentReceived(900));
    }
}
