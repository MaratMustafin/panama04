@extends ('layout')

@section ('content')

<h1>Email Form</h1>
<form action="/email" method="post">
    @csrf
    <input type="email" placeholder="Tap" name="email">
    @error('email')
        <div>{{ $message }}</div>   
    @enderror
    <button type="submit">Send email</button>
    @if (session('message'))
        <div>{{ session('message')}}</div>        
    @endif
</form>
@endsection