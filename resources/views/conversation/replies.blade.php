@foreach ($conversation->replies as $reply) 
    <div>
        <p><strong>{{ $reply->user->name }} said.. </strong></p>
        {{ $reply->body }}
        @can ('update-conversation',$conversation)
        <form action="">
            <button type="submit"> Best Reply?</button>
        </form>
        @endcan
    </div>

    @continue($loop->last)

    <hr>
@endforeach