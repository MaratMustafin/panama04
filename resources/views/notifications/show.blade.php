@extends ('layout')

@section ('content')
<ul>
@forelse ($notifications as $notification)
    <li>{{ $notification->type }}</li>
    @empty
        <p>Нету уведомлений</p>
@endforelse
</ul>
@endsection