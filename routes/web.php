<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// app()->bind('example',function() {
//     return new App\Example;
// });

Route::get('/', function () {
    // $example = resolve('example');
    // ddd($example);
    // // $container = new \App\Container();

    // // $container->bind('example', function() {
    // //     return new App\Example;
    // // });
    

    // // $example = $container->resolve('example');

    // // ddd($example);
    return view('welcome');
});


Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('email','EmailController@show')->name('email');
Route::post('email','EmailController@store');
Route::get('payment','PaymentController@home')->middleware('auth');
Route::post('payment','PaymentController@store')->middleware('auth');
Route::post('payment','PaymentController@store')->middleware('auth');
Route::get('notifications','UserNotificationsController@show')->middleware('auth');
Route::get('conversations','ConversationsController@index');
Route::get('conversations/{conversation}','ConversationsController@show');
